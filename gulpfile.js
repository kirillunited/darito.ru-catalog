const gulp = require('gulp');
const pug = require('gulp-pug');
var spritesmith = require('gulp.spritesmith');
var gulpif = require('gulp-if');

gulp.task('sprite', function () {
    var spriteData = gulp.src('src/img/*.{png,jpg}').pipe(spritesmith({
        imgName: '../img/catalog/cat-sprite.png',
        cssName: '_cat-sprite.css'
    }));
    return spriteData.pipe(gulpif('*.png', gulp.dest('dist/assets/img/catalog/'), gulp.dest('src/scss/')));
});

gulp.task('pug', function () {
    return gulp.src(['src/pug/*.pug', '!./node_modules/**'])
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['pug'], function () {
    gulp.watch('src/**/*', ['pug']);
});

gulp.task('clear', function () {
    return cache.clearAll();
})

gulp.task('default', ['watch']);