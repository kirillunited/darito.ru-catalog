function ImgShw (ID, width, height, alt) {
    var scroll = "no";
    var top = 0, left = 0;
    var w, h;
    if (navigator.userAgent.toLowerCase().indexOf("opera") != -1) {
        w = document.body.offsetWidth;
        h = document.body.offsetHeight;
    } else {
        w = screen.width;
        h = screen.height;
    }
    if (width > w - 10 || height > h - 28)
        scroll = "yes";
    if (height < h - 28)
        top = Math.floor((h - height) / 2 - 14);
    if (width < w - 10)
        left = Math.floor((w - width) / 2 - 5);
    width = Math.min(width, w - 10);
    height = Math.min(height, h - 28);
    var wnd = window.open("", "", "scrollbars=" + scroll + ",resizable=yes,width=" + width + ",height=" + height + ",left=" + left + ",top=" + top);
    wnd.document.write(
        "<html><head>" +
        "<" + "script type=\"text/javascript\">" +
        "function KeyPress(e)" +
        "{" +
        "   if (!e) e = window.event;" +
        "   if(e.keyCode == 27) " +
        "       window.close();" +
        "}" +
        "</" + "script>" +
        "<title>" + (alt == "" ? "Картинка" : alt) + "</title></head>" +
        "<body topmargin=\"0\" leftmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" onKeyDown=\"KeyPress(arguments[0])\">" +
        "<img src=\"" + ID + "\" border=\"0\" alt=\"" + alt + "\" />" +
        "</body></html>"
    );
    wnd.document.close();
    wnd.focus();
}

function AddToBookmark (a) {
    var title = window.document.title; // запоминаем заголовок активной страницы/вкладки
    var url = window.document.location; // адрес тоже запоминаем

    if (window.sidebar) {  // такой объект есть только в Gecko
        window.sidebar.addPanel(title, url, ""); // используем его метод добавления закладки
    }
    else if (typeof(opera) == "object") {  // есть объект opera?
        a.rel = "sidebar"; // добавлем закладку, смотрите вызов функции ниже
        a.title = title;
        a.url = url;
        return true;
    }
    else if (document.all) {  // ну значит это Internet Explorer
        window.external.AddFavorite(url, title); // используем соответсвующий метод
    }
    else {
        alert("Для добавления страницы в Избранное нажмите Ctrl+D"); // для всех остальных браузеров, в т.ч. Chrome
    }

    return false;
}

/* Добавить в Избранное НАЧАЛО */
function addFavoritesHandler ($selector) {
    $selector.on('click', function () {
        var itemId = $(this).attr('data-pid');
        var thisElement = $(this);
        var allFavButton = $(".add-fav[data-pid=" + itemId + "]");

        if (itemId) {
            $.post("https://darito.ru/local/templates/darito/include/ajax/_favorites.php", {
                    id: itemId,
                    count: 1
                },
                function (data) {
                    // data содержит количество
                    //allFavButton.toggleClass("active");

                    if (!thisElement.hasClass("active")) {
                        allFavButton.addClass('active');
                        allFavButton.text('Добавлено в избранное');
                    } else {
                        allFavButton.removeClass('active');
                        allFavButton.text('Добавить в избранное');
                    }

                    $(".fav_count").html(data);
                });
        }
        return false;
    });
}

/* Fixed menu by Enterego START */
function media_resize_flex_menu(){
    if ($('body').width() <= 860) {
        $('.enter_input_search_menu').css('display', 'none');
        $('.menu_b').find('.b_inner').css('display', 'none');
        if ($('body').width() <= 650) {
            $('.enter_logo_menu').css('flex-basis', '400px');
            $('.enter_search_button_menu').css('order', '7');
            $('.enter_flex_menu_box').css('padding', '5px');
            if ($('body').width() <= 550) {
                $('.enter_search_button_menu').css('order', '7');
                $('.enter_logo_menu').css('flex-basis', '300px');
                if ($('body').width() <= 400) {
                    $('.enter_search_button_menu').css('order', '7');
                    $('.enter_logo_menu').css('flex-basis', '200px');
                    if ($('body').width() <= 310) {
                        $('.enter_search_button_menu').css('order', '7');
                        $('.enter_logo_menu').css('flex-basis', '150px');
                    }else{
                        $('.enter_logo_menu').css('flex-basis', '200px');
                    }
                }else{
                    $('.enter_search_button_menu').css('order', '7');
                    $('.enter_logo_menu').css('flex-basis', '300px');
                }
            }else{
                $('.enter_search_button_menu').css('order', '7');
                $('.enter_logo_menu').css('flex-basis', '400px');
            }
        }else{
            $('.enter_logo_menu').css('flex-basis', '200px');
            $('.enter_search_button_menu').css('order', '30');
        }
    }else{
        $('.enter_input_search_menu').css('display', 'block');
        $('.menu_b').find('.b_inner').css('display', 'block');
        $('.enter_logo_menu').css('flex-basis', '200px');
        $('.enter_search_button_menu').css('order', '30');
    }
}

function cancel_search_input(){
    var $search_input_box = $('div.enter_input_search_menu');
    var $enter_phone = $('div.enter_phone_menu');
    var $enter_mail = $('div.enter_mail_menu');
    var $enter_hamburger = $('div.enter_hamburger');
    var $enter_logo = $('a.enter_logo_menu');
    var $cancel_button = $('div.enter_cancel_button_menu');

    //Возвращаем размер input
    var $old_search_input_size = $('input.enter_input_search').attr('old_size');
    $('input.enter_input_search').css('width', $old_search_input_size);

    //Возвращаем кнопку поиска на свое место
    var $old_search_button_order = $('div.enter_search_button_menu').attr('old_order');
    $('div.enter_search_button_menu').css('order', $old_search_button_order);

    $search_input_box.add($cancel_button).hide(300); //Скрываем строку поиска и кнпоку отмены
    //Отображаем прежние элементы меню
    $enter_hamburger.add($enter_logo).add($enter_phone).add($enter_mail).show(300, function(){
        //Запускаем адаптивность после отображения
        media_resize_flex_menu();
    });

}

function fixed_menu() {
    if ($('.menu_b').height() > 0) {
        var p = $('.menu_b').offset().top;
        $(window).scroll(function () {

            /*
            if($('body').width() <= 860){
                if($('div.enter_input_search_menu').is(":visible") == true && $('input.enter_input_search').is( ":focus" ) == false){
                    cancel_search_input();
                }
            }
            */
            if ($(window).scrollTop() >= p) {
                $('.toped').fadeIn(); //Показать кнопку "Наверх"
                $('#enter_fixed_top_menu').show(); //Показать меню
            } else {
                $('.toped').fadeOut(); //Скрыть кнопку "Наверх"
                $('#enter_fixed_top_menu').hide(); //Скрыть меню
            }
        });
        media_resize_flex_menu();
    }
}

function start_search(){
    var $search_input_box = $('div.enter_input_search_menu');
    var $search_cancel_btn = $('div.enter_cancel_button_menu');
    var $enter_phone = $('div.enter_phone_menu');
    var $enter_mail = $('div.enter_mail_menu');
    var $enter_hamburger = $('div.enter_hamburger');
    var $enter_logo = $('a.enter_logo_menu');
    var $search_url;
    var $search_text = $('input.enter_input_search').val();
    var $new_input_width = $(window).width() - 100;

    if($search_input_box.is(":visible") == true){
        if($search_text.length > 0){
            $(location).attr('href','https://darito.ru/search/index.php?q=' + $search_text);
        }else{
            $('input.enter_input_search').focus();
        }
    }else{
        $('input.enter_input_search').attr('old_size', $('input.enter_input_search').css('width')); //Сохраним старый размер INPUT'а в атрибут
        $('input.enter_input_search').css('width', $new_input_width); //Задаем новый размер
        $('div.enter_search_button_menu').attr('old_order', $('div.enter_search_button_menu').css('order')); //Получаем значение сортировки кнопки поиска и записываем в атрибут
        $('div.enter_search_button_menu').css('order','30'); //Устанавливаем значение сортировки для кнопки поска в 30

        //Скрываем ненужные элементы меню
        $enter_hamburger.add($enter_logo).add($enter_phone).add($enter_mail).fadeOut(25, function(){
            $search_cancel_btn.add($search_input_box).show(300, function(){
                $('input.enter_input_search').focus();
            });
        });
    }
}
///////////////////////////////////////////////////////////////////
/////////// AJAX Document Ready Fix by Enterego 202 START /////////
///////////////////////////////////////////////////////////////////

$('body').on('click', '.jcarousel .prev', function () {
    $('.item_jcarousel').jcarousel('scroll', '-=1');
});
$('body').on('click', '.jcarousel .next', function () {
    $('.item_jcarousel').jcarousel('scroll', '+=1');
});

/* кнопки поиска */
$('body').on('click', '.search_btn', function () {
    $('.search_form').addClass('open');
});
$('body').on('click', '.search_close', function () {
    $('.search_form').removeClass('open');
});

$('body').on('click', '#enter_hamburger_ico', function () {
    var $section_box = $("div[class='enter_fixed_top_menu_sections_box']");
    var $hamburger_ico = $("#enter_hamburger_ico");
    if($section_box.is(":visible") == true){
        $($hamburger_ico).removeClass('enter_hamburger_ico_active');
        $($section_box).hide();
    }else{
        $($section_box).show();
        $($hamburger_ico).addClass('enter_hamburger_ico_active');
    }
});

$('body').on('click', 'div.enter_cancel_button_menu', function () {
    cancel_search_input();
});

$('body').on('click', 'div.enter_search_button_menu', function () {
    start_search();
});

$('body').on('keypress', 'input.enter_input_search', function () {
    if(e.keyCode==13){
        start_search();
    }
});

$('body').on('click', '.help_b a[href*="#"]', function (e) {
    var anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top - 51
    }, 1000);
    e.preventDefault();
});

/* Кнопка вверх */
$('body').on('click', '.toped', function () {
    $('html, body').animate({
        scrollTop: 0
    }, 500, function () {
    });
});

/* Узнать подробнее в доставке */

$('body').on('click', '.tabs_b.about .vipparners .box a', function () {
    if ($(this).text() == "Узнать подробнее") {
        $(this).prev().slideDown();
        $(this).text("Скрыть");
    } else {
        $(this).prev().slideUp();
        $(this).text("Узнать подробнее");
    }
    return false;
});

/* Наведение на картинки */

$('body').on('hover', '.new_b .carousel .slide .wrap .img, .content_b .center .itemProduct .img a, .content_b .right .itemProduct .img a, .similar_b .similar .slide .wrap .img', function () {
    var $el = $(this);

    if ($('img', $el).length > 1) {
        $('img[rel="1"]', $el).removeClass('show-item');
        $('img[rel="2"]', $el).addClass('show-item');
    }
}, function () {
    var $el = $(this);

    if ($('img', $el).length > 1) {
        $('img[rel="1"]', $el).addClass('show-item');
        $('img[rel="2"]', $el).removeClass('show-item');
    }
});
/*
 $('.b_outer .product_b .photo').hover(function(){
 var h = $('.product_b .photos .jcarousel .item:nth-child(2)').data('iamge');
 var img = $(this).find('img');
 img.attr('src',h);
 },function(){
 var h = $('.product_b .photos .jcarousel .item:nth-child(1)').data('iamge');
 var img = $(this).find('img');
 img.attr('src',h);
 });
 */
/* Это интересно */

$('body').on('click', '.social-article .right a.blog-more_link', function () {
    var product_id = $(this).attr("product_id");
    var value = parseInt($(this).find("span").text());
    $(this).find("span").text(value + 1);
    $.fancybox({
        type: 'inline',
        href: '.interest-js',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });

    $.post("https://darito.ru/local/templates/darito/include/ajax/interest_up.php", {product_id: product_id}, function () {

    });
    return false;
});


/* Подписка */
$("body").on("click", ".interest-js .form input[type='submitBtn']", function () {
    var email = $(this).parent().find("input[name='email']");
    $.post("https://darito.ru/local/templates/darito/include/ajax/interest_subscribe.php", {email: email}, function () {

    });
    return false;
});

/* отправка форм и валидация */
$('body').on('focus','.form input',function()
{
    $(this).parent().removeClass('feild_wrapper').find('.error_label').remove();
})

$('body').on('click', '.go', function (e) {

    e.stopPropagation();
    e.preventDefault();

    var $form = $(this).closest('.form');
    var $btn = $(this);
    var data = Object();
    var rules = Object();
    var success = $form.data('success');
    if (!success) success = '.success';
    var action = $form.data('action');
    if (!action) action = 'https://darito.ru/local/templates/darito/include/ajax/manage_form.php';

    var inputs = $form.find('input, select, textarea').add('textarea', $form.get(0));
    var validate = true;

    inputs.each(function ()
    {
        var r = $(this).attr('data-rules');

        if (r && r.length != 0) {
            rules[$(this).attr('name')] = r;
            r = r.split(',');

            for (i = 0; i < r.length; i++)
            {
                var rule = r[i];
                if (validator[rule]) {
                    if (!validator[rule]($(this))) {
                        validate = false;
                    }
                }
            }
        }

        data[$(this).attr('name')] = $(this).val();
        $arData = data;

    });
    
    if (!validate) return;

    $btn.addClass('btn-desabled').removeClass('go');

    $.ajax({
        url: action,
        data: data,
        type: 'post',
        dataType: 'JSON',
        success: function (data) {
            
            if (success == ".cupon-success") $(".cupon-success .in").text(data.coupon);
            
            if ($arData['form_id'] == 'buy1click' && typeof yaCounter46583793 != 'undefined')
            {
                yaCounter46583793.reachGoal('оneclicktobuy');
            }
            
            inputs.val(null);
            $btn.addClass('go').removeClass('btn-desabled');

            $.fancybox({
                type: 'inline',
                href: success,
                padding: 0,
                scrolling: 'visible',
                beforeClose: function () {
                    $(".fancybox-skin").addClass('flipOutX animated');
                }
            });
        }
    });

    return false;
});

$('body').on('click', '#rul', function () {
    if ($(this).prev().length)
        $(this).prev().removeClass('wrong');
});

$('body').on('click', '.product_b .photos .item', function () {
    var image = $('.photo img').attr('src');
    var src = $(this).data('iamge');
    if (image != src) {
        $('#circularG').addClass('active');
        $('.b_outer .product_b .photo').unbind('mouseenter mouseleave');
        $('.photo img').attr('src', src);
        $('.photo img').attr('data-zoom-image', src);

        /*var ez =   $('.photo img').data('elevateZoom');*/
        /*ez.swaptheimage(src, src); */

        $($('.photo img')).load(function () {
            $('#circularG').removeClass('active');
        });

    }
});

$('body').on('keyup', '.reg_b input[name=pass2]', function () {
    var pass = $('.reg_b input[name=pass]').val();
    var pass2 = $('.reg_b input[name=pass2]').val();
    if (pass == pass2) {
        $('.reg_b').find('input[name=pass2]').closest('.input').addClass('success').removeClass('error');
    } else {
        $('.reg_b').find('input[name=pass2]').closest('.input').addClass('error').removeClass('success');
    }
});
$('body').on('keyup', '.reg_b input[name=pass]', function () {
    var pass = $('.reg_b input[name=pass]').val();
    if (pass.length > 4) {
        $('.reg_b').find('input[name=pass]').closest('.input').addClass('success').removeClass('error');
    } else {
        $('.reg_b').find('input[name=pass]').closest('.input').addClass('error').removeClass('success');
    }
});

$('body').on('click', '.video_b .left', function () {
    var video = $(this).data('video');
    $(this).addClass('play');
    $(this).addClass('play');
    $(this).html('<iframe width="100%" height="347" src="' + video + '?autoplay=1" frameborder="0" allowfullscreen></iframe>');
});

/* $('.print').on('click', function(){
 window.print()
 }); */

////
$('body').on('click', '.personal_b', function () {
    if (!$(this).hasClass("active")) {
        $(this).addClass('active');
        $(this).text('Перейти');
        $(this).parent().find(".alert-cart").fadeIn();
        setTimeout(function () {
            $(".alert-cart").fadeOut();
        }, 3000);
    } else {
        location.href = 'https://darito.ru/personal/basket/';
    }
});

$('body').on('change','#dostavka_select',function(){
    var price = $("#dostavka_select option:selected").attr('price_val');
    var time = $("#dostavka_select option:selected").attr('time_val');
    if (price.length > 0)
        price = price + " рублей";
    $('#PRICE_DOSTAVKA').html(price);
    $('#TIME_DOSTAVKA').html(time);
});

$('body').on('mouseover','.command_b ul.tabs__caption li',function(){
    $('.command_b ul.tabs__caption li').removeClass('active');
    $(this).addClass('active');
    $('.command_b .tabs .tabs__content').removeClass('active');
    var index=$(this).index();
    $('.command_b .tabs .tabs__content').each(function()
    {
        if($(this).index()==index+1){$(this).addClass('active');}
    })

});

$('body').on("click", ".searchOpen", function () {
    if ($(".search_b .hidden").hasClass("opened")) {
        $(".search_b .hidden").removeClass("opened");
        $(".search_b .shadow").removeClass("active");
    } else {
        $(".search_b .hidden").addClass("opened");
        $(".search_b .shadow").addClass("active");
    }
    return false;
});
$('body').on("click", ".search_b .shadow", function () {
    $(".search_b .hidden").removeClass("opened");
    $(".search_b .shadow").removeClass("active");
    return false;
});

$('body').on("click", ".search .ttl", function () {
    if ($(this).closest('.wrap').find(".content").is(":hidden")) {
        $(this).closest('.wrap').find(".content").show("slow");
    } else {
        $(this).closest('.wrap').find(".content").hide("slow");
    }
    return false;
});

$('body').on("click", "#showHideContent", function () {
    if ($("#content").is(":hidden")) {
        $("#content").show("slow");
        $('.about_b .content').addClass('active');
        $('.author').addClass('active');
    } else {
        $("#content").hide("slow");
        $('.about_b .content').removeClass('active');
        $('.author').removeClass('active');
    }
    return false;
});

/* Смена картинок в попапе карточки товара */
$('body').on("click", ".jcarousel .box_2 .item img", function () {
    var src = $(this).attr("data-iamge");
    $(".fancybox-inner > .photos .dragscroll img").attr({"src": src});
});

/* Смена слайдеров в меню. Блок выполнен хардкодом */
$('body').on('mouseover', '#menu_slider-img .left a', function (e) {
    var src = $(this).attr("image");
    $("#menu_slider-img-block img").attr({"src": src});
});

$('body').on('click', '.menu_b li.parent a', function (e) {
    if ($('body').width() < 800) {
        e.stopPropagation();
        e.preventDefault();
        console.log('click');
        var m = $(this).closest('li').find('.menu');
        if (m.is(":hidden")) {
            m.show("slow");
        } else {
            m.hide("slow");
        }
    }
});

$('body').on('click', '.menu_b .open', function () {
    $(this).closest('.menu_b').toggleClass('active');
});

/* modal */

$('body').on('click', '.video .video-info-js', function () {
    $.fancybox({
        type: 'inline',
        href: '.info-js',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });

});

$('body').on('click', '.product_b .photo', function () {
    $.fancybox({
        type: 'inline',
        content: '<div class="photos c_fix"><div class="zoom-plus"></div><div class="zoom-minus"></div>' + $('.product_b .photos').html() + '</div>',
        scrolling: 'visible',
        beforeLoad: function () {
            $(".fancybox-inner > .photos .photo").draggable();
        },
        afterShow: function () {
            dragscroll.reset();
            $(".fancybox-inner .jcarousel .print-btn").show();
        }
    });

});

$('body').on('click', '.zoom-plus', function () {
    var zoom = $('.product_b .photo').data('zoom');
    zoom = zoom + 10;
    if (zoom > 150) return false;
    $(this).closest('.photos').find('.photo img').css('min-height', +zoom + '%');
    $('.product_b .photo').data('zoom', zoom);
    dragscroll.reset();
});

$('body').on('click', '.zoom-minus', function () {
    var zoom = $('.product_b .photo').data('zoom');
    zoom = zoom - 10;
    if (zoom < 100) return false;
    $(this).closest('.photos').find('.photo img').css('min-height', +zoom + '%');
    $('.product_b .photo').data('zoom', zoom);
    dragscroll.reset();
});

$('body').on('click', '.video .video-youtube-js', function () {
    console.log('iframe');
    $.fancybox({
        type: 'iframe',
        href: 'http://www.youtube.com/embed/' + $(this).data('video') + '?autoplay=1',
        scrolling: 'visible',
        maxWidth: 800,
        maxHeight: 600,
        padding: 0,
        fitToView: false,
        width: '70%',
        height: '70%',
        autoSize: false,
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });

});

$('body').on("click", ".carousel_item .item", function () {
    $('.fancybox-inner > .photos .dragscroll img').attr({'src': $(this).attr('data-iamge')});
    $('.product_b .main_box .photos .photo img').attr({'src': $(this).attr('data-iamge')});
    $(".carousel_item .item").removeClass("active");
    $(this).addClass("active");
    return false;
});

$('body').on('click', ".forgot", function (e) {
    $.fancybox({
        // type : 'inline',
        // href : '.recovery-modal',
        type: 'ajax',
        href: '/login/?forgot_password=yes&is_ajax_auth=y',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});

$('body').on('click', '.recall', function (e) {
    if($(this).hasClass('email')){
        $('.recall-modal .title').text('Написать нам');
        console.log('class find');
    }else{
        $('.recall-modal .title').text('Написать директору');
        console.log('class not find');
    }
    $.fancybox({
        type: 'inline',
        href: '.recall-modal',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});

//АНТИКВАРИАТ Кнопка Оставить заявку START//
$('body').on('click', '.send_me_order_button_ak', function (e) {
    $.fancybox({
        type: 'inline',
        href: '.send_me_order_ak',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});
//АНТИКВАРИАТ Кнопка Оставить заявку END//

//РЕПРИНТ Кнопка Оставить заявку START//
$('body').on('click', '.send_me_order_button_rp', function (e) {
    $.fancybox({
        type: 'inline',
        href: '.send_me_order_rp',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});
//РЕПРИНТ Кнопка Оставить заявку END//

// Элементы декорирования для книг
$('body').on('click', '.decor_link', function (e) {
    $.fancybox({
        type: 'inline',
        href: '.decor',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});

// заказ каталога
$('body').on('click', '.orderCatalog', function (e) {
    $.fancybox({
        type: 'inline',
        href: '#ordercatalog',
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});

$('body').on('click', ".m_login", function (e) {
    e.stopPropagation();
    e.preventDefault();
    $.fancybox({
        // type : 'inline',
        // href : '.login_b',
        type: 'ajax',
        href: '/login/?login=yes&is_ajax_auth=y',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
            $(".fancybox-skin").addClass('zoomIn animated');
            $('body').data('login', '1');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});

$('body').on('click', ".regNew", function (e) {
    e.stopPropagation();
    e.preventDefault();
    $.fancybox({
        // type : 'inline',
        // href : '.reg_b',
        type: 'ajax',
        href: '/login/?register=yes&is_ajax_auth=y',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
            $(".fancybox-skin").addClass('zoomIn animated');
            $('body').data('login', '1');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});

$('body').on('click', '.mapOpen', function (e) {
    e.stopPropagation();
    e.preventDefault();
    $.fancybox({
        type: 'inline',
        href: '.map-modal',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
            $(".fancybox-skin").addClass('zoomIn animated');
            $('body').data('login', '1');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
});

/*start: авторизация и регистрация */
$('body').on('submit', '[name="form_auth"]', function (e){
    var $form = $(this),
        formData = $form.serialize(),
        method = $form.attr('method'),
        url = $form.attr('action');

    if (url.indexOf('is_ajax_auth=y') + 1) {
        e.stopPropagation();
        e.preventDefault();

        $.ajax({
            url: url,
            data: formData,
            dataType: "html",
            method: method,
            beforeSend: function () {
                $form.find('[type="submit"]').text('Авторизую...')
            },
            success: function (data) {
                $form.parents('.login_b').replaceWith(data);
            }
        });
    }
});

$('body').on('submit', '[name="forgot_password"]', function (e) {
    var $form = $(this),
        formData = $form.serialize(),
        method = $form.attr('method'),
        url = $form.attr('action');

    if (url.indexOf('is_ajax_auth=y') + 1) {
        e.stopPropagation();
        e.preventDefault();

        $.ajax({
            url: url,
            data: formData,
            dataType: "html",
            method: method,
            beforeSend: function () {
                $form.find('[type="submit"]').text('Отправляю...')
            },
            success: function (data) {
                $form.parents('.recovery-modal').replaceWith(data);
            }
        });
    }
});

$('body').on('change', '[name="REGISTER[EMAIL]"]', function () {
    $('[name="REGISTER[LOGIN]').val($(this).val());
});
$('body').on('click', '[name="register_submit_button"]', function () {
    $('[name="REGISTER[LOGIN]').val($('[name="REGISTER[EMAIL]"]').val());
});
$('body').on('submit', '[name="regform"]', function (e) {
    var $form = $(this),
        formData = $form.serializeArray(),
        method = $form.attr('method'),
        url = $form.attr('action'),
        $submitBtn = $form.find('[type="submit"]');

    if (!$form.find('[name="regulations"]').is(':checked')) {
        alert('Необходимо согласие с обработкой данных');
        e.preventDefault();
        return;
    }

    if (url.indexOf('is_ajax_auth=y') + 1) {
        e.stopPropagation();
        e.preventDefault();

        formData.push({name: $submitBtn.attr('name'), value: $submitBtn.val()});

        $.ajax({
            url: url,
            data: formData,
            dataType: "html",
            method: method,
            beforeSend: function () {
                $submitBtn.text('Регистрирую...')
            },
            success: function (data) {
                $form.parents('.reg_b').replaceWith(data);
            }
        });
    }
});
/*end: авторизация и регистрация */

/* Карточка товара HOVER на основную картинку НАЧАЛО
 var def_src;
 var pattern_src;
 $('.product_b .photo img').hover(
 function(){
 var def_src = $(this).attr("src");
 var pattern_src = $(this).attr("pattern-src");
 if(pattern_src != "undefined")
 {
 $(this).attr("src", pattern_src);
 $(this).attr("pattern-src", def_src);
 }
 },
 function(){
 if(pattern_src != "undefined")
 {
 $(this).attr("src", def_src);
 $(this).attr("pattern-src", pattern_src);
 }
 });
 /* Карточка товара HOVER на основную картинку КОНЕЦ*/

/* Получим массив с id товаров в корзине */
$('body').on('change','#rul_my',function(){

    $('#klone_rul_my').prop('checked',$(this).is(':checked'));
    if($(this).is(':checked'))
    {
        $(this).parent().removeClass('wrong');
    }
});

$('body').on('change','#rul_my_ak',function() {
    $('#klone_rul_my_ak').prop('checked',$(this).is(':checked'));
    if($(this).is(':checked'))
    {
        $(this).parent().removeClass('wrong');
    }
});

$('body').on('change','#rul_my_rp',function() {
    $('#klone_rul_my_rp').prop('checked',$(this).is(':checked'));
    if($(this).is(':checked'))
    {
        $(this).parent().removeClass('wrong');
    }
});

/* Кнопка удалить в корзине НАЧАЛО */
$("body").on('click', '#basket_items_list .del', function () {

    $.post("https://darito.ru/local/templates/darito/include/ajax/_basket_del.php", {
            id: $(this).attr('product_id')
        },
        function (data) {
            //$("#basket").html(data);

            var data_splited = data.split("||");
            var quantity = data_splited[0];
            var price = data_splited[1];
            $(".order .total").html(quantity);
            $(".order .price").html(price);
            $("#basket_info .total").html(quantity);
            $("#basket_info .price_active").html(price);
        });
    $(this).closest(".item-bascket").remove();
    return false;
});
/* Кнопка удалить в корзине КОНЕЦ */

/* Распечатать */
$('body').on("click", ".print-btn", function () {
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title + '</h1>');
    mywindow.document.write(document.getElementById("print-photos-list").innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
});

//$('.order_b,.ordrm,.ordr').on('click', function () {
//    if (!$(this).hasClass("active")) {
//        $(this).addClass('active');
//        $(this).text('Перейти к оформлению');
//        $(this).parent().find(".alert-cart").fadeIn();
//        setTimeout(function () {
//            $(".alert-cart").fadeOut();
//        }, 3000);
//        /*$('.basket_success').css('display','block');
//         $('.basket_success').addClass('fadeInRight animated');
//
//         setTimeout(function(){
//         $('.basket_success').addClass('fadeOut animated');
//
//         setTimeout(function(){
//         $('.basket_success').removeClass('fadeOut fadeInRight animated');
//         $('.basket_success').css('display','none');
//         }, 1000);
//         }, 3000);*/
//    } else {
//        location.href = '/personal/basket/';
//    }
//});

/* Кнопка купить НАЧАЛО */
$('body').on('click', '.buy-button', function () {
    if (!$(this).hasClass("active")) {
        $(this).addClass('active');
        $(this).text('Перейти к оформлению');
        $(this).parent().find(".alert-cart").fadeIn();
        setTimeout(function () {
            $(".alert-cart").fadeOut();
        }, 3000);

        $.post("https://darito.ru/local/templates/darito/include/ajax/_basket.php", {
                id: $(this).attr('data-pid'),
                count: 1
            },
            function (data) {
                if (data != "") {
                    $("a.basket-text").click();
                    $.post("https://darito.ru/local/templates/darito/include/ajax/basket_line.php", function (data_cart) {
                        $(".header_middle-right .header_person-activity").html(data_cart);

                    });
                    $.post("https://darito.ru/local/templates/darito/include/ajax/basket_line_outline.php", function (data_cart) {
                        $(".header_bot-nav-fixed .header_person-activity").html(data_cart);

                    });
                    $.post("https://darito.ru/local/templates/darito/include/ajax/basket_line_mobile.php", function (data_cart) {
                        $(".header_mobile-icons").html(data_cart);

                    });
                }
            }
        );
    } else {
        location.href = 'https://darito.ru/personal/basket/';
    }

    return false;
});
/* Кнопка купить КОНЕЦ */

/* Кнопка купить в 1 клик НАЧАЛО */
$('body').on('click', '#buy1click', function () {
    ////new.darito.ru/public_html/local/templates/darito/include/ajax/_buy1click.php
    return false;
});
/* Кнопка купить в 1 клик КОНЕЦ */

$('body').on('click', '.tabs a', function (){
    var id = $(this).attr("href");
    $(".tabs a").removeClass("active");
    $(this).addClass("active");
    $(".tabs-cont .tab").hide();
    $(".tabs-cont .tab" + id).show();
    return false;
});

/*редактирование полей в личном кабинете*/
$('body').on('click', '.edit', function (e){
    var $inp = $(this).closest('.form-item').find('input')
    $inp.prop('disabled', false);
    $(this).hide();

    if ($inp.attr('name') == 'NEW_PASSWORD')
        $('#confirm_pass').removeClass('hide');
    return false;
});

/*оформление заказа*/
$('body').on('click', '.submit-order', function (e) {
    e.stopPropagation();
    e.preventDefault();

    var $form = $('#order'),
        inputs = $form.find('input').add('textarea', $form.get(0)),
        action = $(this).data('action');

    if (checkreq(inputs) == 1)
    {
        $('body,html').animate({scrollTop: $form.offset().top - 100}, 200);
        // первый аргумент - функция
        function second_passed() {
            $('.wrong input').parent().removeClass("wrong");
        }
        setTimeout(second_passed, 2500)



        return;
    }
    submitForm('Y');
});

function after_ajax_ready(){

    $(".funci_preim").fancybox({});

    $('.item_jcarousel').jcarousel();

    // ========== tooltip:
    $('.delivery-cart .js-tooltip:not(.tooltipstered)').tooltipster({
        theme  : 'tooltipster-borderless',
        animationDuration: 200,
        delay: 0
    });
    // ========== tooltip;

    // ========== btn-lang:
    var $btnLang = $('.btn-lang');

    $btnLang.on('click', function(){
        if ($(this).hasClass('btn-lang_en')) {
            $.cookie('last-page', location.href, {path: '/'});
            return true;
        } else if ($(this).hasClass('btn-lang_ru')) {
            var lastPage = $.cookie('last-page');

            if (!lastPage.length) {
                lastPage = 'https://darito.ru/';
            }
            window.location.href = lastPage;
        }
    });
    // ========== btn-lang;

    // ========== js-modal-form:
    $(".js-modal-form").fancybox({
        type: 'inline',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            var $btn = $(this.element);
            var $modal = $(this.content);
            var inputUrl = $btn.data('input-url');

            if (inputUrl.length) {
                $('[name="url"]', $modal).val(inputUrl);
            }
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
    // ========== js-modal-form;


    /*$(".zoom").elevateZoom({
     zoomType : "inner",
     cursor: "crosshair"
     });*/

    $(window).resize(function(){
        media_resize_flex_menu();
    });

    fixed_menu();
    /* Fixed menu by Enterego END */

    $('input').add('textarea').on('focus', function (){
        $(this).parent()
            .removeClass('wrong');
    }).each(function () {
        if ($(this).data('rules')) {
            if ($(this).attr('id') == 'rul') {
                $(this).before('<div class="feild_wrapper"><span class="error_label"></span></div>');
            } else {
                $(this).wrap('<div class="feild_wrapper"></div>');
                $(this).parent().append('<span class="error_label"></span>');
            }
        }
    });

    if ($("#filter-box").length > 0) {
        $("#filter-box").slider({
            range: true,
            min: 1500,
            max: 100000,
            values: [20000, 80000],
            slide: function (event, ui) {
                $("#filter-box-item").val(ui.values[0] + " - " + ui.values[1]);
                $("#filter-box span.ui-slider-handle").eq(0).html("<span class='b'><i>" + Format.price(ui.values[0]) + "</i></span>");
                $("#filter-box span.ui-slider-handle").eq(1).html("<span class='b'><i>" + Format.price(ui.values[1]) + "</i></span>");
            }
        });
        $("#filter-box-item").val($("#filter-box").slider("values", 0) +
            " - " + $("#filter-box").slider("values", 1));
        $("#filter-box span.ui-slider-handle").eq(0).html("<span class='b'><i>" + Format.price($("#filter-box").slider("values", 0)) + "</i></span>");
        $("#filter-box span.ui-slider-handle").eq(1).html("<span class='b'><i>" + Format.price($("#filter-box").slider("values", 1)) + "</i></span>");

        $('.catalog_b .item .title').each(function () {
            $(this).html($(this).text().replace(/(\S+)/g, '<span>$1</span>'));
        });
    }
    if ($("#search-box").length > 0) {
        $("#search-box").slider({
            range: true,
            min: 1500,
            max: 100000,
            values: [20000, 80000],
            slide: function (event, ui) {
                $("#search-box-item").val(ui.values[0] + " - " + ui.values[1]);
                $("#search-box span.ui-slider-handle").eq(0).html("<span class='b'><i>" + Format.price(ui.values[0]) + "</i></span>");
                $("#search-box span.ui-slider-handle").eq(1).html("<span class='b'><i>" + Format.price(ui.values[1]) + "</i></span>");
            }
        });
        $("#search-box-item").val($("#search-box").slider("values", 0) +
            " - " + $("#search-box").slider("values", 1));
        $("#search-box span.ui-slider-handle").eq(0).html("<span class='b'><i>" + Format.price($("#search-box").slider("values", 0)) + "</i></span>");
        $("#search-box span.ui-slider-handle").eq(1).html("<span class='b'><i>" + Format.price($("#search-box").slider("values", 1)) + "</i></span>");

        $('.catalog_b .item .title').each(function () {
            $(this).html($(this).text().replace(/(\S+)/g, '<span>$1</span>'));
        });
    }

    $(".various").fancybox({
        maxWidth: 800,
        maxHeight: 600,
        fitToView: false,
        width: '70%',
        height: '70%',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });

    $('.sheets:not(.noautoslide, .vers3)').each(function () {
        $(this).iosSlider({
            autoSlide: true,
            autoSlideTimer: 3000,
            snapToChildren: true,
            scrollbar: false,
            responsiveSlides: true,
            infiniteSlider: true,
            scrollbarDrag: false,
            scrollbarHide: true,
            desktopClickDrag: true,
            navPrevSelector: $(this).closest('.b_inner').find('.prev'),
            navNextSelector: $(this).closest('.b_inner').find('.next')
        });

    });

    $('.sheets.vers3').each(function () {
        $(this).iosSlider({
            autoSlide: true,
            autoSlideTimer: 3000,
            snapToChildren: true,
            scrollbar: false,
            responsiveSlides: true,
            infiniteSlider: true,
            scrollbarDrag: false,
            scrollbarHide: true,
            desktopClickDrag: true,
            navPrevSelector: $(this).closest('.b_inner').find('.prev'),
            navNextSelector: $(this).closest('.b_inner').find('.next')
        });

    });

    $('.sheets.noautoslide').each(function () {
        $(this).iosSlider({
            snapToChildren: true,
            scrollbar: false,
            responsiveSlides: true,
            infiniteSlider: true,
            scrollbarDrag: false,
            scrollbarHide: true,
            desktopClickDrag: true,
            navPrevSelector: $(this).closest('.b_inner').find('.prev'),
            navNextSelector: $(this).closest('.b_inner').find('.next')
        });

    });

    $('.carousel:not(.autoslid)').each(function () {
        $(this).iosSlider({
            snapToChildren: true,
            scrollbar: false,
            responsiveSlides: true,
            infiniteSlider: true,
            scrollbarDrag: false,
            scrollbarHide: true,
            desktopClickDrag: true,
            navPrevSelector: $(this).closest('.b_inner').find('.prev'),
            navNextSelector: $(this).closest('.b_inner').find('.next'),
            onSlideChange: slideContentComplete,
            onSlideComplete: slideHidden
        });

    });

    $('.carousel.autoslid').each(function () {
        $(this).iosSlider({
            snapToChildren: true,
            autoSlide: true,
            autoSlideTimer: 6000,
            scrollbar: false,
            responsiveSlides: true,
            infiniteSlider: true,
            scrollbarDrag: false,
            scrollbarHide: true,
            desktopClickDrag: true,
            navPrevSelector: $(this).closest('.b_inner').find('.prev'),
            navNextSelector: $(this).closest('.b_inner').find('.next'),
            onSlideChange: slideContentComplete,
            onSlideComplete: slideHidden
        });

    });

    $('.carousel .slider .slide:nth-child(1)').addClass('animated');

    function slideContentComplete(args) {
        if (!args.slideChanged) return false;
        $(args.currentSlideObject).addClass('animated');
    }

    function slideHidden(args) {
        if (!args.slideChanged) return false;
        $(args.currentSlideObject).siblings(".slide").removeClass('animated');
    }

    $('.similar').each(function () {
        $(this).iosSlider({
            snapToChildren: true,
            scrollbar: false,
            responsiveSlides: true,
            infiniteSlider: true,
            scrollbarDrag: false,
            scrollbarHide: true,
            desktopClickDrag: true,
            navPrevSelector: $(this).closest('.b_inner').find('.prev'),
            navNextSelector: $(this).closest('.b_inner').find('.next')
        });
    });

    $('.catalog_b .enum .item,.catalog_b .enum .row').each(function () {
        var w = $(this).width();
        var h = $(this).height();
        $(this).data('o', h / w);
    });

    $(".oneclick").fancybox({
        padding: 0,
        width: 'auto',
        scrolling: 'visible',
        afterShow: function () {
            var w = $('body').width(),
                $card = $('.product_b.oneclick-page');
            if (w > 1000) {
                w = 1000;
            } else {
                w = (w * 80) / 100;
            }
            $card.css('width', w + 'px');

            /* Маска на телефон */
            $(".product_b .main_box .info_box .fast input[name='phone']").mask("+7(999) 999-99-99");

            /* Добавить в Избранное */
            addFavoritesHandler($('.add-fav', $card));
        }
    });

    $(".fancybox").fancybox({
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });

    $(".cupon:not(.user), .recommenduser").fancybox({
        type: 'inline',
        href: '.cupon-form',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
    $(".cupon.user").fancybox({
        type: 'inline',
        href: '.cupon-success',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
    $(".orderCatalog").fancybox({
        type: 'inline',
        href: '#ordercatalog',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });

    $(".oneclick-product").fancybox({
        type: 'inline',
        href: '.oneclick-product-modal',
        padding: 0,
        scrolling: 'visible',
        afterLoad: function () {
            $(".fancybox-skin").addClass('zoomIn animated');
            $('.item_jcarousel').jcarousel();
            $('body').on("click", ".jcarousel .prev", function () {
                $('.item_jcarousel').jcarousel('scroll', '-=1');
            });
            $('body').on("click", ".jcarousel .next", function () {
                $('.item_jcarousel').jcarousel('scroll', '+=1');
            });
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });

    if (!window.mobilecheck()) {
        $("input[name=phone]").mask("+7 (999) 999-9999");
    }

    $.post("https://darito.ru/local/templates/darito/include/ajax/_basket_get_items_id.php", {},
        function (data) {
            var pid = data.split(',');

            $('.order_b').each(function () {

                var id = $(this).data('pid');

                for (i in pid) {
                    if (pid[i] == id) {
                        $(this).addClass('active');
                        $(this).text('Перейти к оформлению');
                        return;
                    }
                }

            });
            $('.ordr').each(function () {

                var id = $(this).data('pid');

                for (i in pid) {
                    if (pid[i] == id) {
                        $(this).addClass('active');
                        $(this).text('Перейти к оформлению');
                        return;
                    }
                }

            });
        });

    /* Логика Торговых предложений НАЧАЛО */
    var offers_all_id = $("#offers_all_id").val();
    var offers_all_prices = $("#offers_all_prices").val();
    var offers_all_prices_discount = $("#offers_all_prices_discount").val();
    var offers_all_prices_discount_diff = $("#offers_all_prices_discount_diff").val();
    $('body').on('click', '.offers :checkbox', function () {
        var buy_id = 0;
        var offers_all_id_array_def = offers_all_id.split(",");
        var offers_all_id_array = offers_all_id.split(",");
        var offers_all_prices_array = offers_all_prices.split(",");
        var offers_all_prices_discount_array = offers_all_prices_discount.split(",");
        var offers_all_prices_discount_diff_array = offers_all_prices_discount_diff.split(",");
        var param = [];

        $(".offers :checkbox").each(function () {
            var offers_this_array = $(this).attr('items-id').split(",");
            // Если чекбокс активен, то оставим только совпадающие
            if ($(this).is(':checked')) {
                // убрать то что не равно этим
                var index;
                for (index = offers_all_id_array.length - 1; index >= 0; --index) {
                    if ($.inArray(offers_all_id_array[index], offers_this_array) == "-1") {
                        offers_all_id_array.splice(index, 1);
                    }
                }
            }
            else {
                // убрать то что равно этим
                var index;
                for (index = offers_all_id_array.length - 1; index >= 0; --index) {
                    if ($.inArray(offers_all_id_array[index], offers_this_array) != "-1") {
                        offers_all_id_array.splice(index, 1);
                    }
                }
            }
        });
        var buy_value = $("#buy-button").attr('data-pid-default');
        var price_value = 0;
        var price_discount = 0;
        var price_discount_diff = 0;
        if (offers_all_id_array.length == 1) {
            buy_value = offers_all_id_array[0];
            var index_offer_in_array = offers_all_id_array_def.indexOf(buy_value)
            /* console.log("Индекс: "+index_offer_in_array);
             console.log("Все цены: "+offers_all_prices_array); */

            price_value = offers_all_prices_array[index_offer_in_array];
            price_discount = offers_all_prices_discount_array[index_offer_in_array];
            price_discount_diff = offers_all_prices_discount_diff_array[index_offer_in_array];
        }
        $("#buy-button").attr('data-pid', buy_value);

        $(".price .total i").html(price_discount + " <span class='rubl'>a</span>");
        $(".price .oldprice").html(price_value + " <span class='rubl'>a</span>");
        $(".price .economy").html("Вы экономите - " + price_discount_diff + " <span class='rubl'>a</span>");
        /* console.log("Цена: "+price_value);
         console.log("Скидка: "+price_discount);
         console.log("Разница: "+price_discount_diff); */

        //alert(offers_all_id);
        //alert(offers_all_id_array);


        /* id_prop = $(this).attr('id-skuprop');
         id_offers = $(this).attr('items-id');
         id_offers = id_offers.split(',');


         alert($(this).attr('items-id'));
         alert("Выбран ли: "+$(this).is(':checked'));


         alert(param); */

        /* var arr = ["Яблоко", "Апельсин", "Груша"];
         arr.forEach(function(item, i, arr) {
         alert( i + ": " + item + " (массив:" + arr + ")" );
         }); */

    });
    /* Логика Торговых предложений КОНЕЦ */

    /* Добавить в Избранное */
    addFavoritesHandler($('.add-fav'));


    /* Удалить из Избранного */
    $("#content__").on('click', ".delete-fav", function () {
        //alert($(this).attr('data-pid'));
        var itemId = $(this).attr('data-pid');
        var thisElement = $(this);
        //alert(itemId);
        if (itemId) {
            $.ajax({
                url: "/local/templates/darito/include/ajax/_favorites.php",
                type: "POST",
                data: {
                    id: itemId,
                    //action: 'delete',
                    count: 1
                },
                success: function (data) {
                    $("#fav_count").html(data);
                    $.ajax({
                        url: "/personal/favorites/index.php",
                        type: "POST",
                        success: function (data) {
                            $("#content__").html(data);
                        }
                    });
                }
            });

            /*$.post("/local/templates/darito/include/ajax/_favorites.php",{
             id: itemId,
             action: 'delete',
             count: 1
             },
             function(data){
             $("#fav_count").html(data);
             });*/
        }
        return false;
    });

    /* $('.liked,.liked_b').on('click',function(){
     if(!$(this).hasClass("active")){
     $(this).addClass('active');
     $(this).text('Добавлено в избранное');
     }else{
     $(this).removeClass('active');
     $(this).text('Добавить в избранное');
     }
     }); */
    /* Добавить в Избранное КОНЕЦ */

    $("input.phone").mask("+7 (999) 999-9999");
    $("input[name='phone']").mask("+7 (999) 999-9999");

    $('.map-modal ul.tabs__caption, .tabs_b ul.tabs__caption,.money_b ul.tabs__caption').on('click', 'li:not(.active)', function () {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
}

$(function(){
    after_ajax_ready();
});

BX.addCustomEvent('onAjaxSuccess', function(){
    after_ajax_ready();

    $('#ajax_catalog_main_header').css('display', 'none');
});
///////////////////////////////////////////////////////////////////
/////////// AJAX Document Ready Fix by Enterego 202 END ///////////
///////////////////////////////////////////////////////////////////


Format = {
    price: function (value) {
        return this.format(value, 0, ',', ' ');
    },

    format: function (number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                    .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
}

var validator = {
    required: function ($i) {
        if ($i.val() == '' || $i.val() == $i.attr('placeholder')) {
            fieldError.call($i, lang.requiredError);

            return false;
        }
        return true;
    },
    email: function ($i) {

        if ($i.val() == '') return true;

        var r = new RegExp(".+@.+\..+", "i");
        if (!r.test($i.val())) {
            fieldError.call($i, lang.emailError);
            return false;
        }
        return true;
    },
    phone: function ($i) {
        var r = new RegExp("^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", "i");
        if (!r.test($i.val())) {
            fieldError.call($i, lang.phoneError);
            return false;
        }
        return true;
    },
    ////
    regulations: function ($i) {

        $($i).prev().removeClass('wrong');

        if (!$i.prop("checked")) {
            fieldError.call($i, lang.regulationsError);
            return false;
        }
        return true;
    }
};

var fieldError = function (message) {

    if ($(this).attr('id') == 'rul') {

        $(this).before('<div class="feild_wrapper wrong"><span class="error_label">' + message + '</span></div>');

    }
    else {

        if (!$(this).parent().hasClass('feild_wrapper')) {

            $(this).wrap('<div class="feild_wrapper"></div>');
            $(this).parent().append('<span class="error_label"></span>');

        }
        $(this).parent().addClass('wrong');
        $(this).siblings('.error_label').text(message);
    }


    return false;
};
////
var checkreq = function (inputs) {
    var error = 0;
    var rules = Object();

    inputs.each(function ()
    {
        var r = $(this).data('rules');
        if (r && r.length != 0)
        {
            var dataSid = $(this).data('sid');
            if (dataSid)
                rules[dataSid] = r;
            else
                rules[$(this).attr('name')] = r;
            r = r.split(',');
            for (i = 0; i < r.length; i++) {
                var rule = r[i];
                if (validator[rule])
                {
                    if (!validator[rule]($(this)))
                    {
                        error = 1;
                    }
                }
            }
        }
    });
    return error;
};
////
var lang = {
    success: 'Ваша заявка успешно отправлена',
    error: 'Произошла ошибка, попробуйте еще раз',
    name: 'Имя',
    phone: 'Телефон',
    email: 'Email',
    programs: 'Программы',
    requiredError: 'Это поле не может быть пустым',
    emailError: 'Поле Email должно содержать корректный адрес',
    phoneError: 'Укажите корректный номер телефона',
    regulationsError: 'Необходимо принять условия пользовательского соглашения'
};

window.mobilecheck = function () {
    var check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))check = true
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

(function (e) {
    function t() {
        var e = document.createElement("input"), t = "onpaste";
        return e.setAttribute(t, ""), "function" == typeof e[t] ? "paste" : "input"
    }

    var n, a = t() + ".mask", r = navigator.userAgent, i = /iphone/i.test(r), o = /android/i.test(r);
    e.mask = {
        definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
        dataName: "rawMaskFn",
        placeholder: "_"
    }, e.fn.extend({
        caret: function (e, t) {
            var n;
            if (0 !== this.length && !this.is(":hidden"))return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function () {
                this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (n = this.createTextRange(), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
            })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
                begin: e,
                end: t
            })
        }, unmask: function () {
            return this.trigger("unmask")
        }, mask: function (t, r) {
            var c, l, s, u, f, h;
            return !t && this.length > 0 ? (c = e(this[0]), c.data(e.mask.dataName)()) : (r = e.extend({
                placeholder: e.mask.placeholder,
                completed: null
            }, r), l = e.mask.definitions, s = [], u = h = t.length, f = null, e.each(t.split(""), function (e, t) {
                "?" == t ? (h--, u = e) : l[t] ? (s.push(RegExp(l[t])), null === f && (f = s.length - 1)) : s.push(null)
            }), this.trigger("unmask").each(function () {
                function c(e) {
                    for (; h > ++e && !s[e];);
                    return e
                }

                function d(e) {
                    for (; --e >= 0 && !s[e];);
                    return e
                }

                function m(e, t) {
                    var n, a;
                    if (!(0 > e)) {
                        for (n = e, a = c(t); h > n; n++)if (s[n]) {
                            if (!(h > a && s[n].test(R[a])))break;
                            R[n] = R[a], R[a] = r.placeholder, a = c(a)
                        }
                        b(), x.caret(Math.max(f, e))
                    }
                }

                function p(e) {
                    var t, n, a, i;
                    for (t = e, n = r.placeholder; h > t; t++)if (s[t]) {
                        if (a = c(t), i = R[t], R[t] = n, !(h > a && s[a].test(i)))break;
                        n = i
                    }
                }

                function g(e) {
                    var t, n, a, r = e.which;
                    8 === r || 46 === r || i && 127 === r ? (t = x.caret(), n = t.begin, a = t.end, 0 === a - n && (n = 46 !== r ? d(n) : a = c(n - 1), a = 46 === r ? c(a) : a), k(n, a), m(n, a - 1), e.preventDefault()) : 27 == r && (x.val(S), x.caret(0, y()), e.preventDefault())
                }

                function v(t) {
                    var n, a, i, l = t.which, u = x.caret();
                    t.ctrlKey || t.altKey || t.metaKey || 32 > l || l && (0 !== u.end - u.begin && (k(u.begin, u.end), m(u.begin, u.end - 1)), n = c(u.begin - 1), h > n && (a = String.fromCharCode(l), s[n].test(a) && (p(n), R[n] = a, b(), i = c(n), o ? setTimeout(e.proxy(e.fn.caret, x, i), 0) : x.caret(i), r.completed && i >= h && r.completed.call(x))), t.preventDefault())
                }

                function k(e, t) {
                    var n;
                    for (n = e; t > n && h > n; n++)s[n] && (R[n] = r.placeholder)
                }

                function b() {
                    x.val(R.join(""))
                }

                function y(e) {
                    var t, n, a = x.val(), i = -1;
                    for (t = 0, pos = 0; h > t; t++)if (s[t]) {
                        for (R[t] = r.placeholder; pos++ < a.length;)if (n = a.charAt(pos - 1), s[t].test(n)) {
                            R[t] = n, i = t;
                            break
                        }
                        if (pos > a.length)break
                    } else R[t] === a.charAt(pos) && t !== u && (pos++, i = t);
                    return e ? b() : u > i + 1 ? (x.val(""), k(0, h)) : (b(), x.val(x.val().substring(0, i + 1))), u ? t : f
                }

                var x = e(this), R = e.map(t.split(""), function (e) {
                    return "?" != e ? l[e] ? r.placeholder : e : void 0
                }), S = x.val();
                x.data(e.mask.dataName, function () {
                    return e.map(R, function (e, t) {
                        return s[t] && e != r.placeholder ? e : null
                    }).join("")
                }), x.attr("readonly") || x.one("unmask", function () {
                    x.unbind(".mask").removeData(e.mask.dataName)
                }).bind("focus.mask", function () {
                    clearTimeout(n);
                    var e;
                    S = x.val(), e = y(), n = setTimeout(function () {
                        b(), e == t.length ? x.caret(0, e) : x.caret(e)
                    }, 10)
                }).bind("blur.mask", function () {
                    y(), x.val() != S && x.change()
                }).bind("keydown.mask", g).bind("keypress.mask", v).bind(a, function () {
                    setTimeout(function () {
                        var e = y(!0);
                        x.caret(e), r.completed && e == x.val().length && r.completed.call(x)
                    }, 0)
                }), y()
            }))
        }
    })
})(jQuery);

/* select */

(function ($) {

    function EasyDropDown() {
        this.isField = true,
            this.down = false,
            this.inFocus = false,
            this.disabled = false,
            this.cutOff = false,
            this.hasLabel = false,
            this.keyboardMode = false,
            this.nativeTouch = true,
            this.wrapperClass = 'dropdown',
            this.onChange = null;
    };

    EasyDropDown.prototype = {
        constructor: EasyDropDown,
        instances: {},
        init: function (domNode, settings) {
            var self = this;

            $.extend(self, settings);
            self.$select = $(domNode);
            self.id = domNode.id;
            self.options = [];
            self.$options = self.$select.find('option');
            self.isTouch = 'ontouchend' in document;
            self.$select.removeClass(self.wrapperClass + ' dropdown');
            if (self.$select.is(':disabled')) {
                self.disabled = true;
            }
            ;
            if (self.$options.length) {
                self.$options.each(function (i) {
                    var $option = $(this);
                    if ($option.is(':selected')) {
                        self.selected = {
                            index: i,
                            title: $option.text()
                        }
                        self.focusIndex = i;
                    }
                    ;
                    if ($option.hasClass('label') && i == 0) {
                        self.hasLabel = true;
                        self.label = $option.text();
                        $option.attr('value', '');
                    } else {
                        self.options.push({
                            domNode: $option[0],
                            title: $option.text(),
                            value: $option.val(),
                            selected: $option.is(':selected')
                        });
                    }
                    ;
                });
                if (!self.selected) {
                    self.selected = {
                        index: 0,
                        title: self.$options.eq(0).text()
                    }
                    self.focusIndex = 0;
                }
                ;
                self.render();
            }
            ;
        },

        render: function () {
            var self = this,
                touchClass = self.isTouch && self.nativeTouch ? ' touch' : '',
                disabledClass = self.disabled ? ' disabled' : '';

            self.$container = self.$select.wrap('<div class="' + self.wrapperClass + touchClass + disabledClass + '"><span class="old"/></div>').parent().parent();
            self.$active = $('<span class="selected">' + self.selected.title + '</span>').appendTo(self.$container);
            self.$carat = $('<span class="carat"/>').appendTo(self.$container);
            self.$scrollWrapper = $('<div><ul/></div>').appendTo(self.$container);
            self.$dropDown = self.$scrollWrapper.find('ul');
            self.$form = self.$container.closest('form');
            $.each(self.options, function () {
                var option = this,
                    active = option.selected ? ' class="active"' : '';
                self.$dropDown.append('<li' + active + '>' + option.title + '</li>');
            });
            self.$items = self.$dropDown.find('li');

            if (self.cutOff && self.$items.length > self.cutOff)self.$container.addClass('scrollable');

            self.getMaxHeight();

            if (self.isTouch && self.nativeTouch) {
                self.bindTouchHandlers();
            } else {
                self.bindHandlers();
            }
            ;
        },

        getMaxHeight: function () {
            var self = this;

            self.maxHeight = 0;

            for (i = 0; i < self.$items.length; i++) {
                var $item = self.$items.eq(i);
                self.maxHeight += $item.outerHeight();
                if (self.cutOff == i + 1) {
                    break;
                }
                ;
            }
            ;
        },

        bindTouchHandlers: function () {
            var self = this;
            self.$container.on('click.easyDropDown', function () {
                self.$select.focus();
            });
            self.$select.on({
                change: function () {
                    var $selected = $(this).find('option:selected'),
                        title = $selected.text(),
                        value = $selected.val();

                    self.$active.text(title);
                    if (typeof self.onChange === 'function') {
                        self.onChange.call(self.$select[0], {
                            title: title,
                            value: value
                        });
                    }
                    ;
                },
                focus: function () {
                    self.$container.addClass('focus');
                },
                blur: function () {
                    self.$container.removeClass('focus');
                }
            });
        },

        bindHandlers: function () {
            var self = this;
            self.query = '';
            self.$container.on({
                'click.easyDropDown': function () {
                    if (!self.down && !self.disabled) {
                        self.open();
                    } else {
                        self.close();
                    }
                    ;
                },
                'mousemove.easyDropDown': function () {
                    if (self.keyboardMode) {
                        self.keyboardMode = false;
                    }
                    ;
                }
            });

            $('body').on('click.easyDropDown.' + self.id, function (e) {
                var $target = $(e.target),
                    classNames = self.wrapperClass.split(' ').join('.');

                if (!$target.closest('.' + classNames).length && self.down) {
                    self.close();
                }
                ;
            });

            self.$items.on({
                'click.easyDropDown': function () {
                    var index = $(this).index();
                    self.select(index);
                    self.$select.focus();
                },
                'mouseover.easyDropDown': function () {
                    if (!self.keyboardMode) {
                        var $t = $(this);
                        $t.addClass('focus').siblings().removeClass('focus');
                        self.focusIndex = $t.index();
                    }
                    ;
                },
                'mouseout.easyDropDown': function () {
                    if (!self.keyboardMode) {
                        $(this).removeClass('focus');
                    }
                    ;
                }
            });

            self.$select.on({
                'focus.easyDropDown': function () {
                    self.$container.addClass('focus');
                    self.inFocus = true;
                },
                'blur.easyDropDown': function () {
                    self.$container.removeClass('focus');
                    self.inFocus = false;
                },
                'keydown.easyDropDown': function (e) {
                    if (self.inFocus) {
                        self.keyboardMode = true;
                        var key = e.keyCode;

                        if (key == 38 || key == 40 || key == 32) {
                            e.preventDefault();
                            if (key == 38) {
                                self.focusIndex--
                                self.focusIndex = self.focusIndex < 0 ? self.$items.length - 1 : self.focusIndex;
                            } else if (key == 40) {
                                self.focusIndex++
                                self.focusIndex = self.focusIndex > self.$items.length - 1 ? 0 : self.focusIndex;
                            }
                            ;
                            if (!self.down) {
                                self.open();
                            }
                            ;
                            self.$items.removeClass('focus').eq(self.focusIndex).addClass('focus');
                            if (self.cutOff) {
                                self.scrollToView();
                            }
                            ;
                            self.query = '';
                        }
                        ;
                        if (self.down) {
                            if (key == 9 || key == 27) {
                                self.close();
                            } else if (key == 13) {
                                e.preventDefault();
                                self.select(self.focusIndex);
                                self.close();
                                return false;
                            } else if (key == 8) {
                                e.preventDefault();
                                self.query = self.query.slice(0, -1);
                                self.search();
                                clearTimeout(self.resetQuery);
                                return false;
                            } else if (key != 38 && key != 40) {
                                var letter = String.fromCharCode(key);
                                self.query += letter;
                                self.search();
                                clearTimeout(self.resetQuery);
                            }
                            ;
                        }
                        ;
                    }
                    ;
                },
                'keyup.easyDropDown': function () {
                    self.resetQuery = setTimeout(function () {
                        self.query = '';
                    }, 1200);
                }
            });

            self.$dropDown.on('scroll.easyDropDown', function (e) {
                if (self.$dropDown[0].scrollTop >= self.$dropDown[0].scrollHeight - self.maxHeight) {
                    self.$container.addClass('bottom');
                } else {
                    self.$container.removeClass('bottom');
                }
                ;
            });

            if (self.$form.length) {
                self.$form.on('reset.easyDropDown', function () {
                    var active = self.hasLabel ? self.label : self.options[0].title;
                    self.$active.text(active);
                });
            }
            ;
        },

        unbindHandlers: function () {
            var self = this;

            self.$container
                .add(self.$select)
                .add(self.$items)
                .add(self.$form)
                .add(self.$dropDown)
                .off('.easyDropDown');
            $('body').off('.' + self.id);
        },

        open: function () {
            var self = this,
                scrollTop = window.scrollY || document.documentElement.scrollTop,
                scrollLeft = window.scrollX || document.documentElement.scrollLeft,
                scrollOffset = self.notInViewport(scrollTop);

            self.closeAll();
            self.getMaxHeight();
            self.$select.focus();
            window.scrollTo(scrollLeft, scrollTop + scrollOffset);
            self.$container.addClass('open');
            self.$scrollWrapper.css('height', self.maxHeight + 'px');
            self.down = true;
        },

        close: function () {
            var self = this;
            self.$container.removeClass('open');
            self.$scrollWrapper.css('height', '0px');
            self.focusIndex = self.selected.index;
            self.query = '';
            self.down = false;
        },

        closeAll: function () {
            var self = this,
                instances = Object.getPrototypeOf(self).instances;
            for (var key in instances) {
                var instance = instances[key];
                instance.close();
            }
            ;
        },

        select: function (index) {
            var self = this;

            if (typeof index === 'string') {
                index = self.$select.find('option[value=' + index + ']').index() - 1;
            }
            ;

            var option = self.options[index],
                selectIndex = self.hasLabel ? index + 1 : index;
            self.$items.removeClass('active').eq(index).addClass('active');
            self.$active.text(option.title);
            self.$select
                .find('option')
                .removeAttr('selected')
                .eq(selectIndex)
                .prop('selected', true)
                .parent()
                .trigger('change');

            self.selected = {
                index: index,
                title: option.title
            };
            self.focusIndex = i;
            if (typeof self.onChange === 'function') {
                self.onChange.call(self.$select[0], {
                    title: option.title,
                    value: option.value
                });
            }
            ;
        },

        search: function () {
            var self = this,
                lock = function (i) {
                    self.focusIndex = i;
                    self.$items.removeClass('focus').eq(self.focusIndex).addClass('focus');
                    self.scrollToView();
                },
                getTitle = function (i) {
                    return self.options[i].title.toUpperCase();
                };

            for (i = 0; i < self.options.length; i++) {
                var title = getTitle(i);
                if (title.indexOf(self.query) == 0) {
                    lock(i);
                    return;
                }
                ;
            }
            ;

            for (i = 0; i < self.options.length; i++) {
                var title = getTitle(i);
                if (title.indexOf(self.query) > -1) {
                    lock(i);
                    break;
                }
                ;
            }
            ;
        },

        scrollToView: function () {
            var self = this;
            if (self.focusIndex >= self.cutOff) {
                var $focusItem = self.$items.eq(self.focusIndex),
                    scroll = ($focusItem.outerHeight() * (self.focusIndex + 1)) - self.maxHeight;

                self.$dropDown.scrollTop(scroll);
            }
            ;
        },

        notInViewport: function (scrollTop) {
            var self = this,
                range = {
                    min: scrollTop,
                    max: scrollTop + (window.innerHeight || document.documentElement.clientHeight)
                },
                menuBottom = self.$dropDown.offset().top + self.maxHeight;

            if (menuBottom >= range.min && menuBottom <= range.max) {
                return 0;
            } else {
                return (menuBottom - range.max) + 5;
            }
            ;
        },

        destroy: function () {
            var self = this;
            self.unbindHandlers();
            self.$select.unwrap().siblings().remove();
            self.$select.unwrap();
            delete Object.getPrototypeOf(self).instances[self.$select[0].id];
        },

        disable: function () {
            var self = this;
            self.disabled = true;
            self.$container.addClass('disabled');
            self.$select.attr('disabled', true);
            if (!self.down)self.close();
        },

        enable: function () {
            var self = this;
            self.disabled = false;
            self.$container.removeClass('disabled');
            self.$select.attr('disabled', false);
        }
    };

    var instantiate = function (domNode, settings) {
            domNode.id = !domNode.id ? 'EasyDropDown' + rand() : domNode.id;
            var instance = new EasyDropDown();
            if (!instance.instances[domNode.id]) {
                instance.instances[domNode.id] = instance;
                instance.init(domNode, settings);
            }
            ;
        },
        rand = function () {
            return ('00000' + (Math.random() * 16777216 << 0).toString(16)).substr(-6).toUpperCase();
        };

    $.fn.easyDropDown = function () {
        var args = arguments,
            dataReturn = [],
            eachReturn;

        eachReturn = this.each(function () {
            if (args && typeof args[0] === 'string') {
                var data = EasyDropDown.prototype.instances[this.id][args[0]](args[1], args[2]);
                if (data)dataReturn.push(data);
            } else {
                instantiate(this, args[0]);
            }
            ;
        });

        if (dataReturn.length) {
            return dataReturn.length > 1 ? dataReturn : dataReturn[0];
        } else {
            return eachReturn;
        }
        ;
    };

    $(function () {
        if (typeof Object.getPrototypeOf !== 'function') {
            if (typeof 'test'.__proto__ === 'object') {
                Object.getPrototypeOf = function (object) {
                    return object.__proto__;
                };
            } else {
                Object.getPrototypeOf = function (object) {
                    return object.constructor.prototype;
                };
            }
            ;
        }
        ;

        $('select.dropdown').each(function () {
            var json = $(this).attr('data-settings');
            settings = json ? $.parseJSON(json) : {};
            instantiate(this, settings);
        });
    });
})(jQuery);