// catalog
var initCatSlider = function () {
    $('.js-main-cat_slider').slick({
        arrows: false,
        dots: true,
        autoplay: true
    });
};
var initReceiptsSlider = function () {
    $('.js-receipts_slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        autoplay: true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 860,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 660,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            }
        ]
    });
};
var slideToggle = function (el) {
    el.next().slideToggle('fast');
    el.parent().toggleClass('open');
}
var fadeToggle = function (el) {
    el.next().fadeToggle('fast');
    el.parent().toggleClass('open');
}
var mFilterMenuToggle = function (el) {
    if ($(window).width() > 768) {
        if (el.parent().hasClass('open')) {
            fadeToggle(el);
            return false;
        }
        $('.m-filter_list-wrap:visible').fadeOut('fast');
        $('.m-filter_item').removeClass('open');
        fadeToggle(el);
        return false;
    } else {
        slideToggle(el);
    }
}
// func to hide some tags
var hideTags = function (el) {
    var numOfHiddenTags = 5;
    $(el).each(function (i, element) {
        if (i >= numOfHiddenTags) {
            $(element).not('.cat_tag--more').hide();
        } else if (i < numOfHiddenTags && $(element).is('.cat_tag--more')) {
            $(element).hide();
        }
    });
}
//hide reset filter button
var displayFilterReset = function () {
    var tag = $('[data-tag]');
    var filterReset = $('#del_filter');
    if (tag.length === 0) {
        filterReset.hide();
    } else {
        filterReset.show();
    }
}
$(document).ready(function () {
    var timeoutSlideUp;
    initCatSlider();
    initReceiptsSlider();
    $('body').on('click', '.aside-menu_item .js-toggle', function () {
        var el = $(this);
        slideToggle(el);
        clearTimeout(timeoutSlideUp);
        timeoutSlideUp = setTimeout(function () {
            $(el).parent()
                .siblings()
                .find('.list:visible')
                .slideUp('fast');
            $(el).parent()
                .siblings().removeClass('open');
        }, 6000);
    });
    //subdir
    $('body').on('click', '.filter .js-show-more', function () {
        $(this)
            .siblings('.filter_list')
            .find('.filter_item')
            .show();
        $(this).remove();
        return false;
    });
    // скрываем часть тэгов
    hideTags($('.cat_tag'));
    $('body').on('click', '.js-tag-more, .js-tag-less', function () {
        var showText = 'Показать еще';
        var hideText = 'Скрыть';
        if ($(this).is('.js-tag-more')) {
            $(this).siblings('.cat_tag').show();
            $(this).find('a').text(hideText);
        } else {
            hideTags($(this).siblings('.cat_tag'));
            $(this).find('a').text(showText);
        }
        $(this).toggleClass('js-tag-more js-tag-less');
        return false;
    });
    //show/hide gifts
    var hideGifts = function (el) {
        var numOfShownGifts = 5;
        var gift = $(el).find('.list_item');
        $(gift).each(function (i, element) {
            if (i >= numOfShownGifts) {
                $(element).hide();
            }
        });
        if (gift.length < numOfShownGifts + 1) {
            gift.parents('.cat-gifts_item').find('.more').hide();
        }
    }
    $('.cat-gifts_item').each(function (indexInArray, valueOfElement) {
        hideGifts($(valueOfElement));
    });
    $('body').on('click', '.cat-gifts_item .more, .cat-gifts_item .less', function () {
        var showText = 'Еще категории';
        var hideText = 'Скрыть';
        if ($(this).is('.more')) {
            $(this).siblings('.list').find('.list_item').show();
            $(this).text(hideText);
        } else {
            hideGifts($(this).siblings('.list'));
            $(this).text(showText);
        }
        $(this).toggleClass('more less');
        return false;
    });

    $('body').on('click', '.m-filter .js-toggle', function (event) {
        var el = $(this).parent();
        mFilterMenuToggle(el);
        return false;
    });
    $('body').on('click', '.m-filter_toggle', function (event) {
        $(this).parent().next().slideToggle('fast');
        $(this).toggleClass('active');
        return false;
    });
    $(window).resize(function () {
        if ($(window).width() > 768) {
            $('.m-filter_b.select').removeAttr('style');
            $('.m-filter_toggle').removeClass('active');
        }
    });
    $('body').on('change', '.m-filter_item--sort input', function () {
        var drop = $(this).parents('.m-filter_item--sort');
        drop.find('.name a').text($(this).val());
        drop.find('.m-filter_list-wrap').fadeOut('fast');
        drop.removeClass('open');

        $.ajax({
            url: "subcat.html",
            dataType: "html",
            beforeSend: function () {
                $('.subdir .cat_list .preloader').fadeIn('fast').find('img').fadeIn('fast');
            },
            success: function (html) {
                $('.cat_list > .cols:not(.cat_tags)').html($(html).find('#catList > .cols:not(.cat_tags)').html());
                hideTags($('.cat_tag'));
                $('.cat_tag--more').removeClass('js-tag-less');
                $('.cat_tag--more').addClass('js-tag-more').find('a').text('Показать еще');
                displayFilterReset();
                preloader($('.subdir .cat_list .preloader'));
            }
        });
    });
    $(document).click(function (e) {
        if (!$('.m-filter_item').is(e.target) && $('.m-filter_item').has(e.target).length === 0) {
            $('.m-filter_list-wrap').fadeOut('fast');
            $('.m-filter_item').removeClass('open');
        };
    });
    $('body').on('click', '.js-del-tag', function () {
        var tagID = $(this).parent().attr('data-tag');
        var inp = $('.m-filter').find('[id=' + tagID + ']');
        inp.prop('checked', false);
        if (inp.is('[type=text]')) {
            inp.val('');
            inp.change();
        }
        $(this).parent().remove();
        return false;
    });
    $('body').on('click', '.js-del-all-tags', function () {
        var tag = $(this).parent().find('[data-tag]');
        tag.each(function () {
            var tagID = $(this).attr('data-tag');
            var inp = $('.m-filter').find('[id=' + tagID + ']');
            inp.prop('checked', false);
            if (inp.is('[type=text]')) {
                inp.val('');
                inp.change();
            }
            tag.remove();
        });
        displayFilterReset();
        return false;
    });
    $('body').on('input', '[data-attr=tag] [type=checkbox]', function () {
        var tagID = $(this).attr('id');
        var content = '<li class="m-filter_item m-filter_item--selected" data-tag="' + tagID + '"><a class="js-del-tag" href="#"><span>' + $(this).val() + '</span></a></li>';

        if ($(this).is('[name=priceVar]')) {
            $('[name=price]').val($('[name=priceVar]').val());
        }

        if ($(this).prop('checked') === true) {
            $('.m-filter .selected').append(content);
        } else if ($(this).prop('checked') === false) {
            $('.m-filter .selected').find('[data-tag="' + tagID + '"]').remove();
        }
    });
    $('body').on('input change', '[data-attr=tag] [type=text]', function () {
        // var tagID = $(this).attr('id');
        var tagID = 'priceTag';
        if (!(/^[-+]?\d*\.?\d*$/.test($(this).val()))) {
            $(this).val('');
            return false;
        }
        // var selectVal = $('.m-filter_item--price [data-attr=tag]').attr('data-label=label');
        // var selectValFrom = $('[data-attr="от"]').val();
        // var selectValTo = $('[data-attr="до"]').val();
        // var selectVal = selectValFrom + selectValTo;
        // // inp.each(function (i, el) {
        // //     if ($(this).is('[data-attr="от"]')) {
        // //         var replace = $(this).val().toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        // //         var val = $(this).attr('data-attr') + ' ' + replace + ' р. ';
        // //         selectVal[i] = val;
        // //     } else if ($(this).is('[data-attr="до"]')) {
        // //         var replace = $(this).val().toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        // //         var val = $(this).attr('data-attr') + ' ' + replace + ' р. ';
        // //         selectVal[i] = val;
        // //     }
        // // });
        // console.log(selectVal);

        // var content = '<li class="m-filter_item m-filter_item--selected" data-tag="' + tagID + '"><a class="js-del-tag" href="#"><span>' + selectVal + '</span></a></li>';
        // var tag = $('.m-filter .selected').find('[data-tag="' + tagID + '"]');
        // if (tag.length === 1 && $(this).val() !== '') {
        //     tag.find('a span').text(selectVal);
        // } else if (tag.length === 0 && $(this).val() !== '') {
        //     $('.m-filter .selected').append(content);
        // } else if ($(this).val() === '') {
        //     tag.remove();
        // }
        // $('[data-attr=price] input').prop('checked', false);
    });
    //select price range
    $('body').on('input', '[data-attr=price] input', function () {
        var selectValFrom = $(this).attr('data-value-from');
        var selectValTo = $(this).attr('data-value-to');
        var inp = $(this).parent().siblings('[data-attr=tag]').find('input');
        $(this).parent().siblings().find('input:checked').prop('checked', false);
        inp.each(function () {
            if ($(this).is('[data-attr="от"]')) {
                $(this).val(selectValFrom);
            } else if ($(this).is('[data-attr="до"]')) {
                $(this).val(selectValTo);
            }
        });
        $(this).parent().siblings('[data-attr=tag]').attr('data-label', $(this).attr('data-label'));
        inp.change();
    });
    //pagination
    $('body').on('click', '.pagination_link', function () {
        $(this).parent().siblings().find('.pagination_link').removeClass('active');
        $(this).addClass('active');
        return false;
    });
    $('body').on('click', '.js-cat-more', function () {
        $(this).addClass('load');
        return false;
    });
    //preloader
    if ($('.main-cat_slider').hasClass('slick-initialized')) {
        preloader($('.main-cat_banner .preloader'));
    }
    $('.cat_list .cols').ready(function () {
        preloader($('.subdir .cat_list .preloader'));
    });
});
//preloader func
function preloader(elem) {
    elem.find('img').fadeOut('slow').end().delay(400).fadeOut('slow');
}


// end catalog