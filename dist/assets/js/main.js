$(document).ready(function () {
    $('body').on('click', '.card_link-fav', function (e) {
        e.preventDefault();
        $(this).toggleClass('card_link-fav-active');
    });
    $('body').on('click', '.desc_link-full', function (e) {
        e.preventDefault();
        $(this).siblings('.desc_wrap-text').toggleClass('desc_wrap-text-full');
        $(this).children('span').toggleClass('hidden');
        $(this).children('.fa').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
    });
    //valid form
    $("form").submit(function () {
        $(this).find('input, textarea').removeClass('wrong');
        var size = 0;
        $(this).find('input, textarea').each(function () {
            if ($(this).val() != '') {
                $(this).parent().removeClass('wrong');
            } else {
                $(this).parent().addClass('wrong');
                size += 1;
            }
        });
        $(this).find('[type=checkbox]').each(function () {
            if ($(this).prop('checked') == true) {
                $(this).parent().find('label').removeClass('error');
            } else {
                $(this).parent().find('label').addClass('error');
                size += 1;
            }
        });
        if (size > 0) {
            return false;
        };
        $.post(
            $(this).attr('action'),
            $(this).serialize()
        );
    });
    //guarantees modals
    $('.modal_toggle').fancybox({
        type: 'inline',
        scrolling: 'visible',
        closeBtn: false,
        padding: 15,
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
    //review_post
    $('.review_post').fancybox({
        type: 'image',
        scrolling: 'visible',
        closeBtn: '<img src="./anatomiya-knigi/imgs/ico_close.png" alt="">',
        padding: 15,
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
    // end review_post
    //Checkbox modal
    $('.js-checkbox-modal').change(function () {
        if ($(this).prop('checked')) {
            $('input[name="' + $(this).attr('name') + '"]').prop('checked', true);
        } else {
            $('input[name="' + $(this).attr('name') + '"]').removeAttr('checked');
        }
    });
    //Click modal
    $('body').on('click', '.js-click-modal', function (e) {
        $.fancybox({
            type: 'inline',
            href: '.b-image-modal',
            scrolling: 'visible',
            closeBtn: false,
            padding: 0,
            afterLoad: function () {
                $(".fancybox-overlay").addClass('transparent');
            },
            beforeClose: function () {
                $(".fancybox-skin").addClass('flipOutX animated');
            }
        });
    });

    $('body').on('click', '.js-personal-area', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.fancybox({
            type: 'inline',
            href: '.b-form-modal',
            scrolling: 'visible',
            padding: 0,
            afterLoad: function () {
                $(".fancybox-overlay").addClass('transparent');
            },
            beforeClose: function () {
                $(".fancybox-skin").addClass('flipOutX animated');
            }
        });
    });
    //certificate
    $('.b-footer_certificate-b').fancybox({
        type: 'iframe',
        padding: 0,
        scrolling: 'visible',
        autoSize: false,
        width: 971,
        afterLoad: function () {
            $(".fancybox-overlay").addClass('transparent');
        },
        beforeClose: function () {
            $(".fancybox-skin").addClass('flipOutX animated');
        }
    });
    //slider-adv
    $('.slider-adv').slick({
        dots: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        adaptiveHeight: false,
        prevArrow: '<div class="slider-adv_prev" style="background: url(assets/img/down-arrow-left.png) center center no-repeat;"></div>',
        nextArrow: '<div class="slider-adv_next" style="background: url(assets/img/down-arrow-right.png) center center no-repeat;"></div>',
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
            }
        }]
    });
    //review-list slider
    slidersInit('.review_list');
    $(window).resize(function () {
        slidersInit('.review_list');
        hideDots(); //slider-aside
    });
    //add-to-cart
    $('body').on('click', '.addtocart_btn-add', function (e) {
        var th = $('.addtocart_btn-add');
        if (!$(th).hasClass('addtocart_btn-add-active')) {
            e.preventDefault();
            $(th).toggleClass('addtocart_btn-add-active');
            $(th).find('.addtocart_btnwrap').children().toggleClass('hidden');
            var tooltip = $(th).find('.tooltip');
            tooltip.addClass('tooltip-active');
            setTimeout(function () {
                tooltip.removeClass('tooltip-active');
            }, 2000);
        }
    });
    $('body').on('click', '.addtocart_btn-fav', function (e) {
        e.preventDefault();
        $(this).find('.fa').toggleClass('fa-star-o fa-star');
        $(this).toggleClass('addtocart_btn-fav-active');
    });

    //personal-gift-link
    $('body').on('click', '.personal-gift-link', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 65
        }, 1000);
        event.preventDefault();
    });
    //end add-to-cart
    //slider-main
    $('.slider-aside').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        infinite: false,
        vertical: true,
        prevArrow: '<a href="#" class="slider-aside_prev"></a>',
        nextArrow: '<a href="#" class="slider-aside_next"></a>',
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                dots: true,
                arrows: false,
                vertical: false,
                fade: true
            }
        }]
    });
    $('.slider-main').slick({
        slidesToShow: 1,
        fade: true,
        arrows: true,
        dots: false,
        infinite: false,
        arrows: false
    });
    $('body').on('click', '.slider-aside_item', function () {
        var mainSlider = $('.slider-main');
        var slide = $(this).attr('data-slick-index');

        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        mainSlider.slick('slickGoTo', slide);
    })
    $('body').on('click', '.zoom', function (e) {
        e.preventDefault();
        if (!$(this).hasClass('active')) {
            $(".slider-main_item img").imagezoomsl({
                descarea: ".preview-wrap",
                zoomrange: [1, 12],
                magnifiereffectanimate: "fadeIn",
                magnifierborder: "none",
                magnifycursor: "pointer"
            });
        } else if ($(this).hasClass('active')) {
            $(".magnifier, .cursorshade, .tracker, .statusdiv").remove();
        }
        $(this).toggleClass('active');
        $('.preview-wrap').toggleClass('active');
    })
    hideDots()
    //end slider-main
    // BEGIN: download img
    var img = document.getElementsByTagName('img');
    for (i = 0; i < img.length; i++) {
        img[i].oncontextmenu = function () {
            return false;
        }
        img[i].ondragstart = function () {
            return false;
        };
    }
    // END: download img
});
//slider-aside
function hideDots() {
    if ($('.slider-aside').find('.slider-aside_item').length <= 1) {
        $('.slider-aside .slick-dots').addClass('hidden');
    } else if ($('.slider-aside').find('.slider-aside_item').length > 1) {
        $('.slider-aside .slick-dots').removeClass('hidden');
    }
}
// end slider-aside
//review-list slider
function slidersInit(elem) {
    if ($(elem).length > 0 && !$(elem).hasClass("slick-initialized") && $(window).width() < 1200) {
        $(elem).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            adaptiveHeight: true,
            responsive: [{
                breakpoint: 1199,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    } else if ($(elem).length > 0 && $(elem).hasClass("slick-initialized") && $(window).width() >= 1200) {
        $(elem).slick("unslick");
    }
}